{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = [
        pkgs.ansible
        pkgs.python310Packages.psutil
        pkgs.ansible-lint
        pkgs.python310
    ];
    LC_ALL = "C.UTF-8";
    shellHook = "unset name";
}
