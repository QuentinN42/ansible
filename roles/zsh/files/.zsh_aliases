#!/bin/bash

# ls aliases
alias l='exa -1'
alias ll='exa -ahlF'
alias cl='clear && l'
alias cll='clear && ll'

# Python 3 alias
alias p3='python3'
alias p2='python2'
alias p='poetry'
alias jn='jupyter notebook'
alias jpdf='jupyter nbconvert --to pdf --template ~/.config/jupyter/classic.tplx'
alias ipy='python3.10 -m IPython'

# copy paste
alias copy='xsel --clipboard --input'
alias paste='xsel --clipboard --output'
alias bat='batcat'

function slug(){
  # https://gist.github.com/oneohthree/f528c7ae1e701ad990e6
  echo "$1" |\
  iconv -t ascii//TRANSLIT |\
  sed -r 's/[~\^]+//g' |\
  sed -r 's/[^a-zA-Z0-9]+/-/g' |\
  sed -r 's/^-+\|-+$//g' |\
  tr A-Z a-z
}


function tmp(){
  name=$(yaprompt "Folder name : " ~/.yaprompt/tmp.names) || return 1
  folder_name="$(date -I)-$(slug "$name")"
  mkdir -p ~/tmp/${folder_name}
  cd ~/tmp/${folder_name}
}


function r() {
  nb="${1:-64}"
  if [ "$nb" = "64" ]; then
    todo="8"
  elif [ "$nb" = "32" ]; then
    todo="4"
  elif [ "$nb" = "16" ]; then
    todo="2"
  elif [ "$nb" = "8" ]; then
    todo="1"
  else
    echo "Invalid number of digits" >&2
    return 1
  fi
  echo $(curl -s "https://www.random.org/strings/?num=${todo}&len=8&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new") | tr -d '[:space:]'
}


function _r_complete(){
  COMPREPLY=("8" "16" "32" "64")
}


complete -F _r_complete r


# get all my ipv4 addr
alias cip='clear && ip -c a | grep "inet " | grep -v "inet 127\.0\.0\.1/8 scope host lo"'

# trim all whitespaces
alias trim='sed -e "s/^\ *//g" -e "s/\ *$//g"'

# launch some temp dockers
alias ubuntu="docker run -it ubuntu bash"
alias centos="docker run -it centos bash"
alias debian="docker run -it debian bash"
alias arch="docker run -it archlinux bash"
alias alpine="docker run -it alpine:3.14.0"

alias tor='sudo kalitorify'
alias sound="pavucontrol"

exsource()
{
  set -o allexport
  source $@
  set +o allexport
}

# docker
alias dclean='docker system prune -a -f'

# git
alias gf='git fetch --tags --prune'
alias gsl='git stash && gl && git stash pop'
alias gclean='git branch | grep -v "*" | xargs -I % sh -c "git branch -D %"'
alias gpmr='git push --set-upstream origin $(git branch | grep "*" | cut -d" " -f 2) -o merge_request.create -o merge_request.title="$(git branch | grep "*" | cut -b3- | sed -e "s/\//: /g" -e "s/-/ /g")" -o merge_request.assign="$(git config --get user.name)"'
alias grp='gf && g rebase origin/staging && gpf'
alias gc='git commit -s -m'

# automatisation
alias tf=terraform
alias tg=terragrunt
alias a=ansible-playbook

function autocommit(){
  start="$@"
  message=$(yaprompt "> ${start} " ~/.yaprompt/git.commit)
  if [ ! $? -eq 0 ]; then
    return 1
  fi
  gc "${start} ${message}"
}

function gacp() {
  gaa || return 1
  if [ -z "$1" ]; then
    echo "Provide a commit type" >&2
    return 1
  elif [ "$1" = "ci" ];
  then
      autocommit "ci:" || return 1
  elif [ "$1" = "ciw" ];
  then
      autocommit "ci(wip):" || return 1
  elif [ "$1" = "do" ];
  then
      autocommit "docs:" || return 1
  elif [ "$1" = "dow" ];
  then
      autocommit "docs(wip):" || return 1
  elif [ "$1" = "fe" ];
  then
      autocommit "feat:" || return 1
  elif [ "$1" = "few" ];
  then
      autocommit "feat(wip):" || return 1
  elif [ "$1" = "fi" ];
  then
      autocommit "fix:" || return 1
  elif [ "$1" = "fiw" ];
  then
      autocommit "fix(wip):" || return 1
  elif [ "$1" = "re" ];
  then
      autocommit "refactor:" || return 1
  elif [ "$1" = "rew" ];
  then
      autocommit "refactor(wip):" || return 1
  elif [ "$1" = "te" ];
  then
      autocommit "test:" || return 1
  elif [ "$1" = "tew" ];
  then
      autocommit "test(wip):" || return 1
  elif [ "$1" = "ch" ];
  then
      autocommit "chore:" || return 1
  elif [ "$1" = "chw" ];
  then
      autocommit "chore(wip):" || return 1
  else
    echo "Unknown commit type" >&2
    return 1
  fi
  gp || return 1
}

function _gacp_complete(){
  COMPREPLY=("ci" "ciw" "do" "dow" "fe" "few" "fi" "fiw" "re" "rew" "te" "tew" "ch" "chw")
}

complete -F _gacp_complete gacp

function _ask_branch_name() {
  yaprompt "Branch name : " ~/.yaprompt/git.commit
  return $?
}

function gnb() {
  branch_name=$(_ask_branch_name) || return 1
  if [ -z "$1" ];
  then
    start=''
  elif [ "$1" = "ci" ];
  then
    start='ci/'
  elif [ "$1" = "do" ];
  then
    start='docs/'
  elif [ "$1" = "fe" ];
  then
    start='feat/'
  elif [ "$1" = "fi" ];
  then
    start='fix/'
  elif [ "$1" = "re" ];
  then
    start='refactor/'
  elif [ "$1" = "te" ];
  then
    start='test/'
  elif [ "$1" = "ch" ];
  then
    start='chore/'
  else
    echo "Unknown branch type" >&2
    return 1
  fi
  # https://gist.github.com/oneohthree/f528c7ae1e701ad990e6
  full_branch_name="${start}$(slug "${branch_name}")"
  gcb "$full_branch_name" || return 1
  gpsup || return 1
}

function gnbmr() {
  branch_name=$(_ask_branch_name) || return 1
  if [ -z "$1" ];
  then
    start=''
  elif [ "$1" = "ci" ];
  then
    start='ci/'
  elif [ "$1" = "do" ];
  then
    start='docs/'
  elif [ "$1" = "fe" ];
  then
    start='feat/'
  elif [ "$1" = "fi" ];
  then
    start='fix/'
  elif [ "$1" = "re" ];
  then
    start='refactor/'
  elif [ "$1" = "te" ];
  then
    start='test/'
  elif [ "$1" = "ch" ];
  then
    start='chore/'
  else
    echo "Unknown branch type" >&2
    return 1
  fi
  # https://gist.github.com/oneohthree/f528c7ae1e701ad990e6
  full_branch_name="${start}$(slug "${branch_name}")"
  gcb "$full_branch_name" || return 1
  gpmr || return 1
}

function _gnb_complete(){
  COMPREPLY=("ci" "do" "fe" "fi" "re" "te" "ch")
}

function awstoken(){
  ls "${HOME}/.aws/sso/cache" -1t | head -n1 | grep -qE ".*" || aws sso login;
  `aws sso get-role-credentials --role-name="$(cat "${HOME}/.aws/config" | jc --ini | jq -r ".default.sso_role_name")" --account-id="$(cat "${HOME}/.aws/config" | jc --ini | jq -r ".default.sso_account_id")" --access-token="$(cat "${HOME}/.aws/sso/cache/$(ls -1t "${HOME}/.aws/sso/cache" | head -n 1)" | jq -r ".accessToken")" | jq -r '"export AWS_ACCESS_KEY_ID=" + .roleCredentials.accessKeyId + "\nexport AWS_SECRET_ACCESS_KEY=" + .roleCredentials.secretAccessKey + "\nexport AWS_SESSION_TOKEN=" + .roleCredentials.sessionToken'`
}

complete -F _gnb_complete gnb

function ktoken() {
  cat ~/.kube/configs/$1 | yaml2json| jq '.users[0].user.token' -r | copy
}

function _ktoken_complete(){
  COMPREPLY=$(ls -1 ~/.kube/configs/)
}

complete -F _ktoken_complete ktoken
