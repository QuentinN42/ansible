#!/bin/bash

bash 0-pc.sh

axrandr \
     eDP-1 1920x1200    0x0 normal \
    DP-4-1 1920x1080 1920x0 normal \
    DP-4-3 1920x1080 3840x0 normal

ws2output \
    1 DP-4-3 \
    2 DP-4-1 \
    3 eDP-1
